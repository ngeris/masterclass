'use strict';

(function () {
    $("img, a").on("dragstart", function(e) { e.preventDefault(); }); // image no draggable

    $('input[name=phone]').inputmask("+38(099)-99-99-999");  //static mask


    if (document.querySelector('.front-slider__wrap') !== null) {
        var topSlider = new Swiper('.front-slider__wrap .swiper-container', {
            speed: 400,
            spaceBetween: 0,
            loop: true,
            prevButton: '.front-slider__wrap .swiper-button-prev',
            nextButton: '.front-slider__wrap .swiper-button-next',
            pagination: '.front-slider__wrap .swiper-pagination'
        });
    }

    if (document.querySelector('[data-fancybox]') !== null) {
        $("[data-fancybox]").fancybox({

        });
    }

    //navigation phone dropdown
    (function () {
        jQuery('.navigation__phone-btn').on('click', phonesToggle);

        function phonesToggle() {
            var $list = jQuery(this).next('.navigation__phone-list');
            jQuery(this)
                .toggleClass('navigation__phone-btn--close')
                .toggleClass('navigation__phone-btn--open');
            $list.slideToggle('fast');
        }
    })();




    // function frontSliderSize() {
    //     var slider = document.querySelector('.front-slider__wrap'),
    //         search = document.querySelector('.header__search'),
    //         menu = document.querySelector('.header__menu'),
    //         searchWidth = search.clientWidth;
    //    if(slider) {
    //        if($(window).width() > 768) {
    //            slider.style.width = searchWidth + 'px';
    //        } else if($(window).width() < 768) {
    //            slider.style.width = '';
    //
    //        }
    //    }
    // }
    //
    // frontSliderSize();
    // $(window).resize(frontSliderSize);

    function mainMenuDropdown() {

        // main-menu dropdown
        var menu = document.querySelector('.header__menu'),
            search = document.querySelector('.header__search'),
            dropMenu = document.querySelectorAll('.header__menu--dropdown'),
            elemHover = document.querySelectorAll('.header__menu-item'),
            link = document.querySelectorAll('.header__menu-link');

        if (window.innerWidth > 768) {

            for (var i = 0, ll = link.length; i < ll; i++) {
                if (link[i].nextElementSibling !== null) {
                    link[i].classList.add('header__menu-link--children');
                } else {
                    link[i].classList.remove('header__menu-link--children');
                }
            }


            for (var i = 0, el = elemHover.length; i < el; i++) {
                elemHover[i].addEventListener('mouseenter', menuDropdown);
            }
        }
        function menuDropdown() {

            var children = $(this).find('.header__menu--dropdown'),
                menuHeight = menu.clientHeight,
                searchWidth = search.clientWidth;

            for (var i = 0, dl = dropMenu.length; i < dl; i++) {
                if ($(dropMenu[i]).is(':visible')) {
                    children.css({
                        width: searchWidth,
                        height: menuHeight
                    })
                }
            }
        }
    }
    mainMenuDropdown();
    $(window).resize(mainMenuDropdown);

        var sideNav = {
            navTrigger:     jQuery('.hamburger, .g-overlay'),
            navContainer:   jQuery('.header__menu'),
            body:           jQuery('body')
        };

        function toggleMenu() {
            sideNav.navTrigger.on('click', function(e) {
                sideNav.navTrigger.toggleClass('is-active');
                sideNav.navContainer.toggleClass('is-active');
                sideNav.body.toggleClass('is-hidden');
                e.preventDefault();
            });
        }

        function mobileAccordion() {

            var nav             = sideNav.navContainer;
            var clickableItems  = nav.find('.header__menu-link');

            if(clickableItems.next('.header__menu--sub').length > 0) {
                clickableItems.next('.header__menu-item-children').addClass('open');
            } else {
                clickableItems.next('.header__menu-item-children').removeClass('open');
            }
            clickableItems.parent('.header__menu-item-children.open').children('.header__menu--sub').slideDown();


            nav.on('click', '.header__menu-arrow', function (e) {
                e.stopPropagation();
                var element = jQuery(this).parent(),
                    is_open = element.hasClass('open'),
                    exclude = element.parentsUntil('.header__menu--sub', '.header__menu--sub');
                nav.find('.header__menu--sub').not(exclude).slideUp();
                nav.find('.open').not(exclude.parent().parent().find('.open')).removeClass('open');
                if (!is_open) {
                    element.addClass('open').children('.header__menu--sub').slideDown();
                }
                return false;
            });
        }
        toggleMenu();
        mobileAccordion();




    function menuResize() {
        if(window.innerWidth > 768) {
            $('.header__catalog').append($('.header__menu'));
            $('.header__catalog-btn').after($('.hamburger'));
            $('.g-overlay').removeClass('is-active');
            $('.header__menu--sub').removeClass('header__menu--sub').addClass('header__menu--dropdown');

            $('.navigation__menu-list').append($('.header__menu-item--no-children'));
            $('.navigation__menu-list .header_menu-item').children('a').removeClass('header__menu-link');
            $('.navigation__menu-list .header__menu-item').removeClass('header__menu-item header__menu-item--no-children');

            var link = document.querySelectorAll('.header__menu-link');

            for(var i = 0, ll = link.length; i < ll; i++) {
                if(link[i].nextElementSibling !== null) {
                    link[i].classList.add('header__menu-link--children');
                } else {
                    link[i].classList.remove('header__menu-link--children');
                }
            }
            $('.header__menu--dropdown').css('display', '');

        } else if(window.innerWidth < 768) {
            $('.header__bottom').after($('.header__menu'));
            $('.g-overlay').after($('.hamburger'));
            $('.header__menu-link').removeClass('header__menu-link--children');
            $('.header__menu--dropdown').removeClass('header__menu--dropdown').addClass('header__menu--sub');

            $('.navigation__menu-list li').each(function (index,value) {
                $('.header__menu').append($(value));
                $(value).addClass('header__menu-item header__menu-item--no-children');
                $(value).children('a').addClass('header__menu-link');
            });

            $('.header__menu-item:not(".header__menu-item--no-children")').each(function (index,value) {
                var itemChildren = $(value).children('.header__menu--sub');

                if(itemChildren.length > 0) {
                    $(value).addClass('header__menu-item-children');
                    if(!$(value).children('.header__menu-arrow').length) {
                        $(value).prepend('<span class="header__menu-arrow"></span>');
                    }
                }
            });
            $('.header__menu').removeClass('is-active');
            $('.g-overlay').removeClass('is-active');
        }
    }
    menuResize();
    $(window).resize(menuResize);

    $('.header__catalog-btn').on('click',function () {
         $('.header__menu').slideToggle('fast');
    });


    if (document.querySelector('.our-partners__slider') !== null) {
        var partnersSlider = new Swiper('.our-partners__slider .swiper-container', {
            speed: 400,
            spaceBetween: 0,
            loop: true,
            prevButton: '.our-partners__slider .swiper-button-prev',
            nextButton: '.our-partners__slider .swiper-button-next',
            slidesPerView: 4,
            breakpoints: {
                992: {
                    slidesPerView: 3
                },
                600: {
                    slidesPerView: 2
                },
                480: {
                    slidesPerView: 1
                }
            }
        });
    }


    // slider-product
    $(".slider-product").each(function(index, element) {
        var $this = $(this);
        $this.find('.swiper-container').addClass("instance-" + index);
        $this.find(".swiper-button-prev").addClass("btn-prev-" + index);
        $this.find(".swiper-button-next").addClass("btn-next-" + index);
        var swiper = new Swiper(".instance-" + index, {
            speed: 400,
            slidesPerView: 4,
            loop: true,
            nextButton: ".btn-next-" + index,
            prevButton: ".btn-prev-" + index,
            breakpoints: {
                1200: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 2
                },
                480: {
                    slidesPerView: 1
                }
            }
        });
    });

    if (document.querySelector('.slider-product--popular') !== null) {
        var popularSlider = new Swiper(".slider-product--popular .swiper-container", {
            speed: 400,
            slidesPerView: 5,
            spaceBetween: 0,
            loop: true,
            nextButton: ".slider-product--popular .swiper-button-next",
            prevButton: ".slider-product--popular .swiper-button-prev",
            breakpoints: {
                1200: {
                    slidesPerView: 4
                },
                992: {
                    slidesPerView: 3
                },
                768: {
                    slidesPerView: 2
                },
                600: {
                    slidesPerView: 2
                },
                480: {
                    slidesPerView: 1
                }
            }
        });
    }

    // $('.product__select--wholesale').on('click', function () {
    //    $(this).toggleClass('is-active');
    // });


    (function () {
        var container = document.querySelectorAll('.product__select--wholesale');

        if(container) {
            for(var i=0;i<container.length;i++) {
                container[i].addEventListener('click', selectOption);
            }
        }

        function selectOption(e) {
            var output = this.firstElementChild;
            this.classList.toggle('is-active');

            if(e.target.classList.contains('product__select-option-item')) {
                var price = e.target.textContent;
                output.textContent = price;
            }
        }

    })();

    $(".product__select--partners").select2({
        minimumResultsForSearch: -1,
        placeholder: 'Партнерам'
    });

    $('.aside-mobile-btn').on('click',function () {
        $('.inner-box__aside').slideToggle('fast');
    });

    (function () {
        var acc = $('.acc'),
            layout = acc.find('.acc__layout'),
            title = acc.find('.acc__title'),
            content = acc.find('.acc__content'),
            duration = 300;

        content.first().show();

        title.on('click', accInAction);

        function accInAction() {
            if(!$(this).parent().hasClass('acc__layout--active')) {
                layout.removeClass('acc__layout--active');
                $(this).parent().addClass('acc__layout--active');
                content.stop(true, true).slideUp(duration);
                $(this).stop(true, true).next().slideDown(duration);
            } else {
                content.stop(true, true).slideUp(duration);
                $(this).parent().removeClass('acc__layout--active');
            }
        }

    })();

    var hTabs = {
        title: jQuery('.h-tabs .h-tabs__title'),
        content: jQuery('.h-tabs .h-tabs__content')
    };
    function initializehTabs() {
        hTabs.title.on('click', function(e) {
            var id = '#' + jQuery(this).data('id');
            if(!$(this).hasClass('h-tabs__title--active')) {
                hTabs.title.removeClass('h-tabs__title--active');
                hTabs.content.animate({"opacity" : 0}, 200).hide();
                $(this).addClass('h-tabs__title--active');
                $(id).show().animate({"opacity" : 1}, 200);
            }
            e.preventDefault();
        });
    }
    initializehTabs();

    var cartTabs = {
        title: jQuery('.cart-tabs .cart-tabs__title'),
        content: jQuery('.cart-tabs .cart-tabs__content')
    };
    function initializeCartTabs() {
        cartTabs.title.on('click', function(e) {
            var id = '#' + jQuery(this).data('id');
            if(!$(this).hasClass('cart-tabs__title--active')) {
                cartTabs.title.removeClass('cart-tabs__title--active');
                cartTabs.content.animate({"opacity" : 0}, 200).hide();
                $(this).addClass('cart-tabs__title--active');
                $(id).show().animate({"opacity" : 1}, 200);
            }
            e.preventDefault();
        });
    }
    initializeCartTabs();




    $(".count__input").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $('.count__min').on('click', min);
    $('.count__max').on('click', max);

    function min() {
        var input = $(this).parent().find('.count__input'),
            currentVal = parseInt(input.val()),
            newVal = (!isNaN(currentVal) && currentVal > 1) ? currentVal - 1 : 1;
        input.val(newVal);
    }
    function max() {
        var input = $(this).parent().find('.count__input'),
            currentVal = parseInt(input.val()),
            newVal = (!isNaN(currentVal)) ? currentVal + 1 : 1;
        input.val(newVal);
    }


    (function () {
       var cartBtn = $('#cart-submit');

       function btnMove() {

           if($(window).width() < 992 ) {
               $('.cart-page__inner').append(cartBtn);
           } else if($(window).width() > 992 ) {
               $('.cart-content__inner').after(cartBtn);
           }

       }
       btnMove();
       $(window).resize(function () {
           btnMove();
       });

    })();

    (function () {
        function mapScroll() {
            if($(window).width() < 992 ) {
                $('.contacts__map iframe').css("pointer-events", "none");
                var onMapMouseleaveHandler = function (event) {
                    var that = $(this);
                    that.on('click', onMapClickHandler);
                    that.off('mouseleave', onMapMouseleaveHandler);
                    that.find('iframe').css("pointer-events", "none");
                    console.log()
                };
                var onMapClickHandler = function (event) {
                    var that = $(this);
                    that.off('click', onMapClickHandler);
                    that.find('iframe').css("pointer-events", "auto");
                    that.on('mouseleave', onMapMouseleaveHandler);
                };
                $('.contacts__map').on('click', onMapClickHandler);
            }
        }
        mapScroll();
        $(window).resize(function () {
            mapScroll();
        });
    })();


    $('#cart .dropdown-menu').on('click', function (e) {
        e.stopPropagation();
    });
    $('#cart .fancybox-close-small').on('click', function () {
       $('.header__cart').removeClass('open');
    });




    var darkness = document.getElementById('darkness'),
        link = document.querySelector('.header__login-link');



    link.addEventListener('click', function(e) {
            var popup = document.getElementById(link.getAttribute('href').slice(1));
            e.preventDefault();
            popup.querySelector('.popup__close').addEventListener('click', closePopupHandler);
            darkness.addEventListener('click', closeAll);
            popup.style.display = 'block';
            darkness.style.display = 'block';
        });

    function closeAll() {
        $(this).css('display', 'none');
        $(this).parents('body').find('#login').css('display', 'none');
    }

    function closePopupHandler() {
        darkness.style.display = 'none';
        this.parentNode.style.display = 'none';
        this.removeEventListener('click', closePopupHandler);
    }

    // self phone methods
    $.validator.addMethod("minlenghtphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 11;
        },
        "Введите полный номер");
    $.validator.addMethod("requiredphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 1;
        },
        "Это поле обязательно для заполнения");


    $('.popup__form').validate({
        rules: {
            email: {required: true, email: true},
            password: {required: true, minlength: 6}
        },
        messages: {
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'},
            password: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 6 символов'}
        }
    });

    $('#contacts-form').validate({
        rules: {
            name: {required: true, minlength: 2},
            phone: {requiredphone: true, minlenghtphone: true},
            email: {required: true, email: true},
            area: {required: true, minlength: 12}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 2 символа'},
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'},
            area: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 12 символов'}
        }
    });

    $('#aa-block-form').validate({
        rules: {
            name: {required: true, minlength: 2},
            phone: {requiredphone: true, minlenghtphone: true},
            email: {required: true, email: true},
            area: {required: true, minlength: 12}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 2 символа'},
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'},
            area: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 12 символов'}
        }
    });

    $('.registration__form').validate({
        rules: {
            name: {required: true, minlength: 2},
            email: {required: true, email: true},
            password: {required: true, minlength: 6}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 2 символа'},
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'},
            password: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во 6 символов'}
        }
    });

    // product-sliders loop ---- double
    $(".product-slider").each(function(index, element) {
        $(this).find('.top-gallery').addClass("instance-top-" + index);
        $(this).find('.thumb-gallery').addClass("instance-thumbs-" + index);
        $(this).find(".swiper-button-prev").addClass("btn-prev-" + index);
        $(this).find(".swiper-button-next").addClass("btn-next-" + index);

        var galleryTop = new Swiper('.instance-top-' + index, {
            loop: true,
            loopedSlides: 4,
            nextButton: ".btn-next-" + index,
            prevButton: ".btn-prev-" + index,
        });

        var galleryThumbs = new Swiper('.instance-thumbs-' + index, {
            loop: true,
            slidesPerView: 4,
            loopedSlides: 4,
            slideToClickedSlide: true,
            direction: 'vertical',
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    loopedSlides: 4
                },
                480: {
                    direction: 'horizontal'
                }
            }
        });
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
    });

    // catalog aside
    (function () {
        var aside = {
            title:  jQuery('.aside__option-name'),
            content: jQuery('.aside__option-values'),
            arrow: jQuery('.aside__option-name-arrow')
        };
        aside.title.on('click', asideContent);

        function asideContent() {
            $(this).parent().find(aside.content).slideToggle();
            $(this).find(aside.arrow).toggleClass('rotate-active');
        }
    })();


    // product-card-color-options
    (function () {
        var colorItem = document.querySelectorAll('.card-product__content-color-prop'),
            colorAttr,
            label = document.querySelectorAll('.card-product__content-color-item input[type=radio]');

        for(var i=0;i<colorItem.length;i++) {
            colorAttr = colorItem[i].getAttribute('data-color');
            colorItem[i].style.backgroundColor = colorAttr;
            colorItem[i].firstChild.style.color = colorAttr;
        }

        for(var i=0;i<label.length;i++) {

            if(label[i].getAttribute('checked') !== null) {
                if(label[i].nextElementSibling.classList.contains('card-product__content-color-prop--white')) {
                    label[i].nextElementSibling.style.backgroundColor = '#e6e6e6';
                } else {
                    label[i].nextElementSibling.style.borderColor = label[i].nextElementSibling.getAttribute('data-color');
                    label[i].nextElementSibling.style.backgroundColor = 'transparent';
                }
            }

            label[i].addEventListener('change',function () {
                for(var j=0;j<colorItem.length;j++) {
                    colorAttr = colorItem[j].getAttribute('data-color');
                    colorItem[j].style.backgroundColor = colorAttr;
                    colorItem[j].firstChild.style.color = colorAttr;
                }

                if(this.nextElementSibling.classList.contains('card-product__content-color-prop--white')) {
                    this.nextElementSibling.style.backgroundColor = '#e6e6e6';
                } else {
                    this.nextElementSibling.style.borderColor = this.nextElementSibling.getAttribute('data-color');
                    this.nextElementSibling.style.backgroundColor = 'transparent';
                }

            });
        }
    })();

}());


// Window load event used just in case window height is dependant upon images
$(window).bind("load", function() {

    var footerHeight = 0,
        footerTop = 0,
        footer = document.querySelector('.footer');

    positionFooter();

    function positionFooter() {

        footerHeight = footer.clientHeight;
        footerTop = document.documentElement.scrollTop + document.documentElement.clientHeight - footerHeight + "px";

        //DEBUGGING STUFF

         /*console.log("Document height: ", document.body.clientHeight);
         console.log("Window height: ", document.documentElement.clientHeight);
         console.log("Window scroll: ", document.documentElement.scrollTop);
         console.log("Footer height: ", footerHeight);
         console.log("Footer top: ", footerTop);
         console.log("-----------");*/


        if ( (document.body.clientHeight + footerHeight) < document.documentElement.clientHeight) {
            $(footer).css({
                position: "absolute"
            }).stop().animate({
                top: footerTop
            })
        } else {
            $(footer).css({
                position: "static"
            })
        }
    }

    $(window)
        .scroll(positionFooter)
        .resize(positionFooter)

});

$(window).ready(function () {
   $('.front-slider__wrap').css('opacity', 1);
});
